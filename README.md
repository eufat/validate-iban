# Validate - PFC IBAN Validator

Service that determines whether or not an IBAN number is valid. This application will conform with operations as  described in ISO 7064. In details, The algorithm of IBAN validation is as follows:

1. Check that the total IBAN length is correct as per the country (there is a table that summarize it). If not, the IBAN is invalid
2. Move the four initial characters to the end of the string
3. Replace each letter in the string with two digits, thereby expanding the string, where A = 10, B = 11, ..., Z = 35
4. Interpret the string as a decimal integer and compute the remainder of that number on division by 97

If the remainder is 1, the check digit test is passed and the IBAN might be valid.

Example (fictitious United Kingdom bank, sort code 12-34-56, account number 98765432):

- IBAN : GB82 WEST 1234 5698 7654 32	
- Rearrange : W E S T12345698765432 G B82	
- Convert to integer : 3214282912345698765432161182	
- Compute remainder : 3214282912345698765432161182	mod 97 = 1

The table summarises the IBAN formats by country is available on [here](https://en.wikipedia.org/wiki/International_Bank_Account_Number). Adaptation of the table is a requirement to check as per country and available in [bban.tsv](./bban.tsv) file.

## Quickstart
Using this commands to build and start the application:

```bash
$ go build -o ~/validate ./cmd/validate && ~/validate start
```

Then, we can use the application to validate an IBAN number:

```bash
$ curl --location --request POST 'localhost:8080/validate' \
--header 'Content-Type: application/json' \
--data-raw '{
    "iban": "GB82WEST12345698765432"
}'
```

Or get the latest IBAN number tested (defaulted to an example):

```bash
$ curl --location --request GET 'localhost:8080/validate' \
--data-raw ''
```

## Tests
We can test the application with the following command:

```bash
$ go test -timeout 10s ./...
```

## Docker
To build the application and run it in a docker container, we can use the following commands:

```bash
$ docker build -t validate-iban:0.1.0-alpha1 .
$ docker run -it --rm -p 8080 --name validate_iban validate-iban:0.1.0-alpha1 start
```

## Deployment
To deploy the application in a Kubernetes cluster, we can use the following command:

```bash
$ kubectl apply -f ./deployment/k8s-deployment.yaml
```
Apply a service so the deployment can be accessed:

```bash
$ kubectl apply -f ./deployment/k8s-service.yaml
```

## Usage
The usage of this application is as follows:

```
NAME:
   validate - PFC IBAN Validator

USAGE:
   validate [global options] command [command options] [arguments...]

VERSION:
   0.1.0

COMMANDS:
   start    Start PFC Validate API
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

### Start
To start the application, is as follows:

```
NAME:
   validate start - Start PFC Validate API

USAGE:
   validate start [command options] [arguments...]

OPTIONS:
   --verbose                Turn on verbose logging and tracing. [$VALIDATE_VERBOSE]
   --log-format value       json/kv log format (default: "json") [$VALIDATE_LOG_FORMAT]
   --prometheus-addr value  The IP address/hostname and port to server prometheus metrics. (default: "0.0.0.0:2112") [$VALIDATE_PROMETHEUS_ADDR]
```