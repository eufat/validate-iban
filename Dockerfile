FROM golang:1.18-alpine AS base

RUN apk update

WORKDIR /app/validate

# Download dependencies
COPY go.mod .
COPY go.sum .
RUN go mod download

# Build executable
FROM base AS builder
WORKDIR /app/validate
COPY . .
RUN go build -o /go/bin/validate ./cmd/validate

# Build runtime package
FROM alpine AS runtime
RUN apk update && apk add make git vim build-base && rm -rf /var/cache/apk/*
COPY --from=builder /go/bin/validate /go/bin/validate

ENTRYPOINT ["/go/bin/validate"]
