package runtimeutils

import (
	"io"

	log "github.com/sirupsen/logrus"
	"github.com/uber/jaeger-client-go"
	"github.com/uber/jaeger-lib/metrics"

	jaegercfg "github.com/uber/jaeger-client-go/config"
	jaegerlog "github.com/uber/jaeger-client-go/log"
)

// InitTracing initialize jaeger tracing options.
func InitTracing(verbose bool) (io.Closer, error) {
	cfg := jaegercfg.Configuration{}

	if verbose {
		cfg.Sampler = &jaegercfg.SamplerConfig{
			Type:  jaeger.SamplerTypeConst,
			Param: 1,
		}
	}

	// Initialize tracer with a logger and a metrics factory
	closer, err := cfg.InitGlobalTracer(
		"validate",
		jaegercfg.Logger(jaegerlog.StdLogger),
		jaegercfg.Metrics(metrics.NullFactory),
	)

	if err != nil {
		log.WithFields(log.Fields{
			"module": "runtimeutils",
			"error":  err.Error(),
		}).Error("could not initialize jaeger tracer")

		return nil, err
	}

	return closer, nil
}
