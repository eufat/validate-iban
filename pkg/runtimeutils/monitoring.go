package runtimeutils

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	log "github.com/sirupsen/logrus"
)

// ServePrometheus starts a http handler for serving prometheus metrics.
func ServePrometheus(address string) {
	http.Handle("/metrics", promhttp.Handler())

	go func() {
		if err := http.ListenAndServe(address, nil); err != nil {
			log.WithFields(log.Fields{
				"module": "runtimeutils",
				"error":  err.Error(),
			}).Error("unable to start http server for prometheus")
		}
	}()
}
