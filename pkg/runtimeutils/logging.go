package runtimeutils

import log "github.com/sirupsen/logrus"

// InitLogging initialize stuctured logging options.
func InitLogging(verbose bool, logFormat string) {
	if verbose {
		log.SetLevel(log.DebugLevel)
	}

	if logFormat == "json" {
		log.SetFormatter(&log.JSONFormatter{})
	}
}
