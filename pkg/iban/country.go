package ibanv

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

type Country struct {
	CountryCode string
	CountryName string
	Chars       int
	BbanFormat  string
}

func ValidateCountry(iban string) bool {
	// strip spaces on IBAN string
	iban = strings.Replace(iban, " ", "", -1)

	// read data from CSV file
	csvFile, err := os.Open("./bban.tsv")

	if err != nil {
		fmt.Println(err)
	}

	defer csvFile.Close()

	reader := csv.NewReader(csvFile)
	reader.Comma = '\t' // Use tab-delimited instead of comma
	reader.FieldsPerRecord = -1

	csvData, err := reader.ReadAll()
	if err != nil {
		log.WithFields(log.Fields{
			"module": "ibanv",
			"error":  err.Error(),
		}).Error("could not read csv file")
		os.Exit(1)
	}

	// create a map with countryCode as key and Country struct as value
	countryMap := make(map[string]Country)

	// iterate over csvData and create a map with countryCode as key
	for _, each := range csvData {
		chars, err := strconv.Atoi(each[1])
		if err != nil {
			fmt.Println(err)
		}

		countryMap[each[0]] = Country{
			CountryCode: each[0],
			CountryName: each[3],
			Chars:       chars,
			BbanFormat:  each[2],
		}
	}

	// get countryCode from iban
	countryCode := strings.ToUpper(iban[0:2])

	fmt.Printf("Checking IBAN: %s with country format: {%s %s %d %s}\n",
		iban,
		countryMap[countryCode].CountryCode,
		countryMap[countryCode].CountryName,
		countryMap[countryCode].Chars,
		countryMap[countryCode].BbanFormat)

	// check if countryCode exists in map
	if _, ok := countryMap[countryCode]; ok {
		ctry := countryMap[countryCode]

		// check if iban length is correct
		if len(iban) == ctry.Chars {
			// retrieve BBAN format and length from ctry
			bbanFormat := ctry.BbanFormat

			/*
				the format of the BBAN part of an IBAN in terms of upper case alpha characters (A–Z) denoted by "a",
				numeric characters (0–9) denoted by "n" and mixed case alphanumeric characters (a–z, A–Z, 0–9) denoted by "c".
				For example, the Bulgarian BBAN (4a,6n,8c) consists of 4 alpha characters,
				followed by 6 numeric characters, then by 8 mixed-case alpha-numeric characters
			*/

			bbanFormatSlice := strings.Split(bbanFormat, ",")
			for _, format := range bbanFormatSlice {
				charactersLength := strings.Split(format, "")[0]

				// convert charactersLength to int
				clength, err := strconv.Atoi(charactersLength)
				if err != nil {
					fmt.Println(err)
					os.Exit(1)
				}

				charactersType := strings.Split(format, "")[1]

				switch charactersType {
				case "a":
					if !strings.ContainsAny(iban[int(clength):], "ABCDEFGHIJKLMNOPQRSTUVWXYZ") {
						fmt.Println("IBAN country format have invalid characters, expected an alpha characters with length: ", clength)
						return false
					}
				case "n":
					if !strings.ContainsAny(iban[int(clength):], "0123456789") {
						fmt.Println("IBAN country format have invalid characters, expected a numeric characters with length: ", clength)
						return false
					}
				case "c":
					if !strings.ContainsAny(iban[int(clength):], "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789") {
						fmt.Println("IBAN country format have invalid characters, expected a mixed-case characters with length: ", clength)
						return false
					}
				}
			}

			fmt.Println("IBAN country format is valid!")
			return true
		} else {
			fmt.Println("IBAN country format invalid length!")
			return false
		}
	} else {
		fmt.Println("IBAN country format not found!")
		return false
	}
}
