package ibanv

import (
	"fmt"
	"math/big"
	"strconv"
	"strings"
	"unicode"
)

func toNum(in rune) int {
	return int(in - 'A' + 1)
}

func ValidateIban(iban string) bool {
	// strip spaces on IBAN string
	iban = strings.Replace(iban, " ", "", -1)

	valid := false

	// move the four initial characters to the end of the string
	initial := iban[:4]
	iban = iban[4:]
	iban = iban + initial

	// replace each letter in the string with two digits, thereby expanding the string
	for _, c := range iban {
		if unicode.IsLetter(c) {
			numC := toNum(rune(c)) + 9 // where A = 10, B = 11, ..., Z = 35
			stringNumC := strconv.Itoa(numC)

			// replace letter to 2 number within IBAN string
			iban = strings.Replace(iban, string(c), stringNumC, 1)
		}
	}

	fmt.Println("IBAN after conversion: ", iban)

	// interpret the string as a decimal integer and compute the remainder of that number on division by 97
	bignumIban, _ := new(big.Int).SetString(iban, 10) // convert string to bignum since Uint can't even handle
	bignum97 := big.NewInt(97)                        // convert also 97
	remainder := new(big.Int).Mod(bignumIban, bignum97)

	bignum1 := big.NewInt(1)

	// If the remainder is 1, the check digit test is passed and the IBAN might be valid.
	if remainder.Cmp(bignum1) == 0 {
		fmt.Println("mod-97 operation succeed, IBAN is valid!")
		valid = true
	}

	return valid
}
