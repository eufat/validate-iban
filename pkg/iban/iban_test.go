package ibanv

import (
	"testing"
)

func TestValidateIban(t *testing.T) {
	// test with invalid IBAN
	if ValidateIban("GB82WEST12345698765440") {
		t.Error("Expected false, got true")
	}

	// test with valid IBAN
	if !ValidateIban("GB82WEST12345698765432") {
		t.Error("Expected true, got false")
	}
}

func TestValidateIbanTrimSpaces(t *testing.T) {
	// test with IBAN that has spaces
	if !ValidateIban("GB82 WEST 1234 5698 7654 32") {
		t.Error("Expected true, got false")
	}
}

func TestToNum(t *testing.T) {
	if toNum('A')+9 != 10 {
		t.Error("Expected 10, got ", toNum('A'))
	}

	if toNum('B')+9 != 11 {
		t.Error("Expected 11, got ", toNum('B'))
	}

	if toNum('Z')+9 != 35 {
		t.Error("Expected 35, got ", toNum('Z'))
	}
}
