package ibanv

import (
	"os"
	"path"
	"runtime"
	"testing"
)

func init() {
	_, filename, _, _ := runtime.Caller(0)
	dir := path.Join(path.Dir(filename), "../../")
	err := os.Chdir(dir)
	if err != nil {
		panic(err)
	}
}

func TestValidateCountry(t *testing.T) {
	// test with valid IBAN
	if !ValidateCountry("GB82WEST12345698765432") {
		t.Error("Expected true, got false")
	}
}

// test ValidateCountry trimming with spaces in IBAN
func TestValidateCountryTrimSpaces(t *testing.T) {
	// test with IBAN that has spaces
	if !ValidateCountry("GB82 WEST 12345698765432") {
		t.Error("Expected true, got false")
	}
}

func TestValidateCountryInvalidCountryCode(t *testing.T) {
	// test with invalid IBAN
	if ValidateCountry("AA82WEST12345698765432") {
		t.Error("Expected false, got true")
	}
}

func TestValidateCountryInvalidLength(t *testing.T) {
	// test with invalid IBAN
	if ValidateCountry("GB82WEST1234569876543") {
		t.Error("Expected false, got true")
	}
}

// test ValidateCountry function invalid characters
func TestValidateCountryInvalidCharacters(t *testing.T) {
	// test with invalid IBAN
	if ValidateCountry("GB82WEST12345698765432A") {
		t.Error("Expected false, got true")
	}
}

// test ValidCountry with Swedish IBAN
func TestValidateCountrySwedish(t *testing.T) {
	// test with valid IBAN
	if !ValidateCountry("SE3550000000054910000005") {
		t.Error("Expected true, got false")
	}
}

// wrong alphanumeric characters in Bulgarian IBAN
func TestValidateCountryCharactersBulgarian(t *testing.T) {
	// test with valid IBAN
	if !ValidateCountry("BG80BNBG96611020345679") {
		t.Error("Expected true, got false")
	}
}

// wrong alphanumeric characters in Czech IBAN
func TestValidateCountryCharactersCzech(t *testing.T) {
	// test with valid IBAN
	if !ValidateCountry("CZ6508000000192000145398") {
		t.Error("Expected true, got false")
	}
}

// wrong alphanumeric characters in Danish IBAN
func TestValidateCountryCharactersDanish(t *testing.T) {
	// test with valid IBAN
	if !ValidateCountry("DK5000400440116242") {
		t.Error("Expected true, got false")
	}
}
