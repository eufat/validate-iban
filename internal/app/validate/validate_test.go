package validate

import (
	"io/ioutil"
	"net/http"
	"testing"
	"time"
)

// run Start function, and hit the /validate endpoint to test
func TestStart(t *testing.T) {
	// use go routine to start the server, use options as parameters
	go Start(&Options{})

	// sleep for 100ms to give the server time to start
	// this is not a good way to do this, but it's a quick and dirty way to test
	time.Sleep(100 * time.Millisecond)

	// hit the /validate endpoint to test with GET
	resp, err := http.Get("http://localhost:8080/validate")
	if err != nil {
		t.Errorf("failed to hit endpoint: %v", err)
	}

	// check the response status code
	if resp.StatusCode != http.StatusOK {
		t.Errorf("expected status code %v, got %v", http.StatusOK, resp.StatusCode)
	}

	// check the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Errorf("failed to read body: %v", err)
	}

	// check the response body is equal to the testIban
	if string(body) != "{\"iban\":\"GB82WEST12345698765432\"}" {
		t.Errorf("expected body %v, got %v", "{\"iban\":\"GB82WEST12345698765432\"}", string(body))
	}
}
