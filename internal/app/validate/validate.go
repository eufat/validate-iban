package validate

import (
	"encoding/json"
	"fmt"
	"net/http"

	log "github.com/sirupsen/logrus"
	ibanv "gitlab.com/eufat/validate/pkg/iban"

	"gitlab.com/eufat/validate/pkg/runtimeutils"
)

type Options struct {
	Verbose   bool
	LogFormat string
	Config    string
}

type iban struct {
	Iban string `json:"iban"`
}

type valid struct {
	Valid bool `json:"valid"`
}

var testIban *iban = &iban{
	Iban: "GB82WEST12345698765432",
}

func validationHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	switch r.Method {
	case "GET":
		j, _ := json.Marshal(testIban)
		w.Write(j)
	case "POST":
		// decode the request body into iban struct
		d := json.NewDecoder(r.Body)
		i := &iban{}
		err := d.Decode(i)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.WithFields(log.Fields{
				"module": "validate",
				"error":  err.Error(),
			}).Error("could not decode request body")
		}

		testIban = i

		// validate IBAN from request body
		v := &valid{
			Valid: ibanv.ValidateCountry(i.Iban) && ibanv.ValidateIban(i.Iban),
		}

		// encode the response body to JSON
		j, err := json.Marshal(v)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			log.WithFields(log.Fields{
				"module": "validate",
				"error":  err.Error(),
			}).Error("could not marshal response")
		}

		w.Write(j)
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		log.WithFields(log.Fields{
			"module": "validate",
			"error":  fmt.Sprintf("method %s not allowed", r.Method),
		}).Error("method not allowed")
	}
}

// Start initializes validate.
func Start(opts *Options) error {
	// initalize logging with options from opts
	runtimeutils.InitLogging(opts.Verbose, opts.LogFormat)

	// initiate tracing with closer and error handling
	closer, err := runtimeutils.InitTracing(opts.Verbose)
	if err != nil {
		log.WithFields(log.Fields{
			"module": "validate",
			"error":  err.Error(),
		}).Error("could not initialize Jaeger tracer")
	}

	// use closer to close the tracer
	defer closer.Close()

	http.HandleFunc("/validate", validationHandler)

	log.Println("Validate service started on port 8080 ...")
	http.ListenAndServe(":8080", nil)

	return nil
}
