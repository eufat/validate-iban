package main

import "github.com/urfave/cli"

var (
	validateFlags = []cli.Flag{
		cli.BoolFlag{
			Name:   "verbose",
			Usage:  "Turn on verbose logging and tracing.",
			EnvVar: "VALIDATE_VERBOSE",
		},
		cli.StringFlag{
			Name:   "log-format",
			Value:  "json",
			Usage:  "json/kv log format",
			EnvVar: "VALIDATE_LOG_FORMAT",
		},
		cli.StringFlag{
			Name:   "prometheus-addr",
			Value:  "0.0.0.0:2112",
			Usage:  "The IP address/hostname and port to server prometheus metrics.",
			EnvVar: "VALIDATE_PROMETHEUS_ADDR",
		},
	}
)
