package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/eufat/validate/internal/app/validate"
)

func main() {
	app := cli.NewApp()
	app.Name = "validate"
	app.Usage = "PFC IBAN Validator"
	app.Version = "0.1.0"
	app.Commands = []cli.Command{
		{
			Name:   "start",
			Usage:  "Start PFC Validate API",
			Flags:  validateFlags,
			Action: validateAction,
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.WithFields(log.Fields{
			"module": "validate",
			"error":  err.Error(),
		}).Fatal("failed to start the CLI")
	}
}

func validateAction(c *cli.Context) error {
	opts := &validate.Options{
		LogFormat: c.String("log-format"),
		Verbose:   c.Bool("verbose"),
	}

	return validate.Start(opts)
}
